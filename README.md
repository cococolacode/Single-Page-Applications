## Single-Page Applications
A single-page application (SPA) is a website that re-renders its content in response to navigation actions (e.g. clicking a link) without making a request to the server to fetch new HTML.

### How Single Page Applications(SPA) works?
<img src="https://user-images.githubusercontent.com/47861774/53687632-eafa4080-3d5e-11e9-8024-9b4190cc5f24.jpg" height="350px"/></br>
The **#** and what comes after it, is called a fragment identifiers - that basically identifies the adject point or portion in html documents.</br>
In the above figure, the fragment identifier **#jack** identifies the **jack** in the same html document and likewise **#mike** do.</br></br>

![url](https://user-images.githubusercontent.com/47861774/53695561-3e16d680-3de5-11e9-8405-1fcd4179e300.png)</br>
![image](https://user-images.githubusercontent.com/47861774/53695544-19bafa00-3de5-11e9-8117-2200c5e45f88.png)


 We can also listen to hashchange event, that is fired everytime when a **hash value/fragment identifiers** changes.let's have a look on how we can listen a hashchange event form url location by using javascript.
 ```javascript
window.addEventListener('hashchange',function(){
console.log(window.location.hash);
});
```
**Output:**</br>
![console1](https://user-images.githubusercontent.com/47861774/53694860-8c73a780-3ddc-11e9-9dc5-963a107fd90f.png)</br>

## Single-Page Application in AngularJS
AngularJs constructs single page application which involves several fundamental concepts
1. Routing
2. Templates
3. Controllers
